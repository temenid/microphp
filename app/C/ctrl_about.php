<?php
class Ctrl_About extends Ctrl{
	function __construct(){
		$this->M=new Model_About();
		$this->V=new View();
	}
	function index(){
		$this->V->render('about.php','outer.php',$this->M->getData());
	}
}
