<?php
class Ctrl_Main extends Ctrl{
	function __construct(){
		$this->M=new Model_Main();
		$this->V=new View();
	}	
	function index(){
		$this->V->render('main.php','outer.php',$this->M->getData());
	}
}
